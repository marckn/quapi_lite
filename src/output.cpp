#include "output.h"
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace pt = boost::property_tree;

void outrhot(std::string fname, std::vector<arma::cx_mat>& rhot, double dt)
{
	std::ofstream outp(fname);
	int nstates = rhot[0].n_rows;
	// write header
    outp << boost::format("#%=10s ") % "1";
    for (int i = 0; i < nstates; i++)
        for (int j = 0; j < nstates; j++)
            outp << boost::format("\t%=12s\t%=12s") % (2 * (i * nstates + j) + 2) % (2 * (i * nstates + j) + 3);
    
    outp << std::endl;
    outp << boost::format("%=12s") % "time";
    for (int i = 0; i < nstates; i++)
        for (int j = 0; j < nstates; j++)
            outp << boost::format("\t%=12s\t%=12s") % ("Re[rho_" + std::to_string(i) + std::to_string(j) + "]") %
                        ("Im[rho_" + std::to_string(i) + std::to_string(j) + "]");
     outp << std::endl;

    // write data
	for(int i=0;i<rhot.size();i++)
	{
		double ct = dt*i;

        outp << boost::format("%+.5e") % ct;
        for (int j = 0; j < nstates; j++)
            for (int k = 0; k < nstates; k++)
                outp << boost::format("\t%+.5e\t%+.5e") % std::real(rhot[i](j,k)) % std::imag(rhot[i](j,k));
		outp<<std::endl;
	}


}
void outsweep(std::string fname, std::vector<std::vector<arma::cx_mat>>& sweep_data, Input& inp)
{
	pt::ptree tree;
	tree.put("data.general_properties.nstates",inp.Nstates);
	double dt = inp.dt;
	int ntpoints=inp.Nbeads;
	if(inp.L!=-1){
		dt = inp.sweep_tf/inp.L;
		ntpoints = inp.L+1;
	}
	tree.put("data.general_properties.nbeads",ntpoints);
	
	tree.put("data.general_properties.dt",dt);
	tree.put("data.general_properties.hbar",inp.hbar);

	
	tree.put("data.dvr_defs.<xmlattr>.diagterm",inp.H0diagpart);
	tree.put("data.dvr_defs.<xmlattr>.dvrdim",inp.dvr_values.n_cols);
	
	for(int i=0;i<inp.Nstates;i++)
	{
		std::string dvrout="";
		for(int d=0;d<inp.dvr_values.n_cols;d++)
			dvrout += std::to_string(inp.dvr_values(i,d))+"  ";

		tree.put("data.dvr_defs.state_"+std::to_string(i),dvrout);
		//tree.put("data.dvr_defs.state_"+std::to_string(i),inp.dvr_values(i));
	}

	for(int i=0;i<inp.Nstates;i++)
		for(int j=0;j<inp.Nstates;j++)
			tree.put("data.hamiltonian.elem_"+std::to_string(i)+"_"+std::to_string(j),inp.H0(i,j));
	for(int i=0;i<ntpoints;i++)
	{
		std::string subnode = "data.sweep.time_index_"+std::to_string(i);
		for(int j=0;j<inp.Nstates*inp.Nstates;j++)
		{
			int i1 = j % inp.Nstates;
			int i2 = j / inp.Nstates;
			std::string sector = subnode + ".sweep_sector_"+std::to_string(j);
			std::string itag = sector+".<xmlattr>.i";
			std::string jtag = sector+".<xmlattr>.j";
			tree.put(itag,i1);
			tree.put(jtag,i2);
			for(int k=0;k<inp.Nstates;k++)
				for(int l=0;l<inp.Nstates;l++)
				{
					std::string rhotag = sector+".rhoelem_"+std::to_string(k)+"_"+std::to_string(l);
					tree.put(rhotag+".real",std::real(sweep_data[j][i](k,l)));
					tree.put(rhotag+".imag",std::imag(sweep_data[j][i](k,l)));
						
				}
		}
	}	
	
	boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
	pt::write_xml(fname,tree,std::locale(),settings);
}


void outsweep_human(std::string fname, std::vector<std::vector<arma::cx_mat>>& sweep_data, Input& inp)
{
	std::ofstream outp(fname);

	double dt = inp.dt;
	int ntpoints=inp.Nbeads;
	if(inp.L!=-1){
		dt = inp.sweep_tf/inp.L;
		ntpoints=inp.L+1;
	}

	for(int i=0;i<ntpoints;i++)
	{
		

		outp<<"#dt = "<<i*dt<<std::endl;
		for(int k=0;k<inp.Nstates;k++)
			for(int l=0;l<inp.Nstates;l++)
			{
				for(int j=0;j<inp.Nstates*inp.Nstates;j++)
				{
					int i1 = j % inp.Nstates;
					int i2 = j / inp.Nstates;
					double re_rho = std::real(sweep_data[j][i](k,l));
					double im_rho = std::imag(sweep_data[j][i](k,l));
					outp<<re_rho<<" "<<im_rho<<"  \t  ";
				}
				outp<<std::endl;

			}

		outp<<std::endl;
		
	}	
}

