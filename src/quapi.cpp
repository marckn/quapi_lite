#include "quapi.h"
#include "path_counter.h"
#include<cmath>
#include<mpi.h>

void quapi_cluster(Input& inp, arma::cx_mat& eta_inner, arma::cx_mat& eta_outer, 
			std::vector<arma::cx_mat>& rhot, unsigned long int cl_start, unsigned long int cl_end)
{
	int nstates = inp.Nstates;
	int nlevels = nstates*nstates;
	int nbeads = inp.Nbeads;
	int mpirank,nranks;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);
	
	arma::vec dvrprods(nstates);
	for(int i=0;i<nstates;i++)
		dvrprods(i) = dvr_prod(inp.dvr_values.row(i),inp.dvr_values.row(i));

	arma::cx_mat prop = arma::expmat(std::complex<double>(0,-inp.dt/inp.hbar)*(inp.H0 - 
		arma::diagmat(dvrprods*inp.H0diagpart)));
		//arma::diagmat(inp.dvr_values%inp.dvr_values*inp.H0diagpart)));
	arma::cx_mat propc = prop.t();
	
	// init forward-backward levels
	std::vector<std::pair<int,int>> FBlevels;
	for(int i=0;i<nstates;i++)
		for(int j=0;j<nstates;j++)
		{
			std::pair<int,int> clv = std::make_pair(i,j);
			FBlevels.push_back(clv);
		}

	unsigned long int npaths = cl_end - cl_start;
	unsigned long int nchunk = npaths / nranks;
	int nrem = npaths % nranks;

	unsigned long int my_start = cl_start;
	
	for(int i=0;i<mpirank;i++)
	{
		unsigned long int nspan = nchunk;
		if(i<nrem)
			nspan++;
		my_start += nspan;
	}

	unsigned long int my_npaths = nchunk;
	if(mpirank<nrem)
		my_npaths++;

	PathCounter paths(nlevels,nbeads);
	if(my_start>0) 
	{
		// a tricky one: need to define "startfrom" correctly 
		//from the previous path in order to avoid double-counting
		paths.jump(my_start-1);
		paths++;
	}else
		paths.jump(my_start);
	
	for(unsigned long int i=0;i<my_npaths;i++)
	{
		
		std::complex<double> freep_weight=inp.rho0(FBlevels[paths[0]].first,FBlevels[paths[0]].second);
		//double s0f = inp.dvr_values(FBlevels[paths[0]].first);
		//double s0b = inp.dvr_values(FBlevels[paths[0]].second);
		//double deltas0 = s0f - s0b;

		arma::rowvec s0f = inp.dvr_values.row(FBlevels[paths[0]].first);
		arma::rowvec s0b = inp.dvr_values.row(FBlevels[paths[0]].second);
		arma::rowvec deltas0 = s0f - s0b;

		//std::complex<double> eta_acc=deltas0*(s0f*eta_inner(0,0) - s0b*std::conj(eta_inner(0,0)));
		std::complex<double> eta_acc=dvr_prod(deltas0,s0f)*eta_inner(0,0) - dvr_prod(deltas0,s0b)*std::conj(eta_inner(0,0));
				
		int startfrom = paths.getDeltaFrom();
		for(int j=1;j<nbeads;j++)
		{
			int jfow = FBlevels[paths[j]].first;
			int jmfow = FBlevels[paths[j-1]].first;
			int jback = FBlevels[paths[j]].second;
			int jmback = FBlevels[paths[j-1]].second;

			std::complex<double> cw = prop(jfow,jmfow)*propc(jmback,jback);
			freep_weight *= cw;

			//double sjf = inp.dvr_values(FBlevels[paths[j]].first);
			//double sjb = inp.dvr_values(FBlevels[paths[j]].second);
			//double deltasj = sjf - sjb;
			arma::rowvec sjf = inp.dvr_values.row(FBlevels[paths[j]].first);
			arma::rowvec sjb = inp.dvr_values.row(FBlevels[paths[j]].second);
			arma::rowvec deltasj = sjf - sjb;
			

			if(j>=startfrom)
			{
				std::complex<double> ceta = eta_acc;
				for(int kp=0;kp<=j;kp++)
				{
					//double skpf = inp.dvr_values(FBlevels[paths[kp]].first);
					//double skpb = inp.dvr_values(FBlevels[paths[kp]].second);
					arma::rowvec skpf = inp.dvr_values.row(FBlevels[paths[kp]].first);
					arma::rowvec skpb = inp.dvr_values.row(FBlevels[paths[kp]].second);


					//ceta += deltasj*(skpf*eta_outer(j-1,kp) - skpb*std::conj(eta_outer(j-1,kp))); 
					ceta += dvr_prod(deltasj,skpf)*eta_outer(j-1,kp) - dvr_prod(deltasj,skpb)*std::conj(eta_outer(j-1,kp)); 
				}
				rhot[j](jfow,jback) += freep_weight*std::exp(-ceta/inp.hbar);
			}

			for(int kp=0;kp<=j;kp++)
			{
				//double skpf = inp.dvr_values(FBlevels[paths[kp]].first);
				//double skpb = inp.dvr_values(FBlevels[paths[kp]].second);
				//eta_acc += deltasj*(skpf*eta_inner(j,kp) - skpb*std::conj(eta_inner(j,kp))); 

				arma::rowvec skpf = inp.dvr_values.row(FBlevels[paths[kp]].first);
				arma::rowvec skpb = inp.dvr_values.row(FBlevels[paths[kp]].second);
				eta_acc += dvr_prod(deltasj,skpf)*eta_inner(j,kp) - dvr_prod(deltasj,skpb)*std::conj(eta_inner(j,kp)); 


			}
		}
		paths++;
	}
}

std::vector<arma::cx_mat> parallel_reduce(std::vector<arma::cx_mat>& rhot)
{
	int nlevels = rhot[0].n_elem;
	int nstates = rhot[0].n_rows;
	int mpirank;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
	std::vector<std::complex<double>> buffer;
	for(int i=1;i<rhot.size();i++)
		for(int j=0;j<nlevels;j++)
			buffer.push_back(rhot[i](j));
	

	if(mpirank!=0)
		MPI_Reduce(&buffer[0],&buffer[0],buffer.size(),MPI_C_DOUBLE_COMPLEX,MPI_SUM,0,MPI_COMM_WORLD);
	else
		MPI_Reduce(MPI_IN_PLACE,&buffer[0],buffer.size(),MPI_C_DOUBLE_COMPLEX,MPI_SUM,0,MPI_COMM_WORLD);

	std::vector<arma::cx_mat> retval;
	retval.push_back(rhot[0]);
	int knt=0;
	for(int i=1;i<rhot.size();i++)
	{
		arma::cx_mat rt(nstates,nstates,arma::fill::zeros);
		for(int j=0;j<nlevels;j++)
			rt(j) = buffer[knt++];
		retval.push_back(rt);
	}

	return retval;


}

std::vector<arma::cx_mat> lite_quapi(Input& inp, arma::cx_mat& eta_inner, arma::cx_mat& eta_outer)
{
	int nstates = inp.Nstates;
	int nlevels = nstates*nstates;
	int nbeads = inp.Nbeads;
	
	
	arma::cx_mat rht(inp.Nstates,inp.Nstates,arma::fill::zeros);
	std::vector<arma::cx_mat> retval(inp.Nbeads,rht);
	retval[0] = inp.rho0;


	if(std::pow((long double)nlevels,nbeads) > std::numeric_limits<unsigned long int>::max())
	{
		std::cout<<"Number of paths exceeds numeric limit"<<std::endl;
		std::exit(1);
	}

	unsigned long int npaths = std::pow((long double)nlevels,nbeads);
	for(int i=0;i<nlevels;i++)
	{
		int i1 = i/nstates;
		int i2 = i%nstates;
		if(std::abs(inp.rho0(i1,i2))<1E-14)
			continue;

		std::vector<int> linit(inp.Nbeads,0);
		linit[0]=i;
		PathCounter paths(linit,nlevels);
		unsigned long int iinit = paths.getIdx();
		unsigned long int iend = iinit + npaths/nlevels;
		quapi_cluster(inp,eta_inner,eta_outer,retval,iinit,iend);
	}



	return parallel_reduce(retval);
}



double dvr_prod(const arma::rowvec& p1, const arma::rowvec& p2)
{
	double prod=0;
	for(int i=0;i<p1.n_elem;i++)
		prod += p1(i)*p2(i);

	return prod;
}