#include<armadillo>
#include<utility>

namespace sambarta_area
{
	std::pair<arma::cx_mat,arma::cx_mat> eta_coeff_wapper(arma::vec& freq, arma::vec& jdw, int Nmax, double beta, double hbar, double dt);
}