#ifndef __OUTPUT__
#define __OUTPUT__
#include<armadillo>
#include<iostream>
#include "input.h"

void outrhot(std::string fname, std::vector<arma::cx_mat>& rhot, double dt);
void outsweep(std::string fname, std::vector<std::vector<arma::cx_mat>>& sweep_data, Input& inp);
void outsweep_human(std::string fname, std::vector<std::vector<arma::cx_mat>>& sweep_data, Input& inp);

#endif