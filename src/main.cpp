#include "input.h"
#include "output.h"
#include "eta_coeffs.h"
#include "quapi.h"
#include<mpi.h>

int main(int argc, char* argv[])
{
	MPI_Init(NULL,NULL);
    int mpirank;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
    //MPI_Comm_size(MPI_COMM_WORLD,&nranks);


	std::string ifile = argv[1];
	Input inp(ifile);

	std::pair<arma::cx_mat,arma::cx_mat> etas = 
		sambarta_area::eta_coeff_wapper(inp.omegas, inp.jdw, inp.Nbeads, inp.beta, inp.hbar, inp.dt);

	if(!inp.sweep)
	{
		std::vector<arma::cx_mat> rhot = lite_quapi(inp, etas.first, etas.second);
		if(mpirank==0)
			outrhot("rho_t.dat",rhot,inp.dt);
	}
	else
	{
		std::vector<std::vector<arma::cx_mat>> sweep_data;
		if(mpirank==0)
			std::cout<<"***INIT DENSM SWEEP"<<std::endl;
		for(int i=0;i<inp.Nstates;i++)
			for(int j=0;j<inp.Nstates;j++)
			{
				if(mpirank==0)
				{
					std::cout<<"\t ==ELEM ["<<i+1<<"/"<<inp.Nstates<<"]["<<j+1<<"/"<<inp.Nstates<<"] ... ";
					std::cout.flush();
				}
				inp.rho0.zeros(inp.Nstates,inp.Nstates);
				inp.rho0(i,j)=1;
				
				if(inp.L==-1){
					std::vector<arma::cx_mat> rhot = lite_quapi(inp, etas.first, etas.second);  
					sweep_data.push_back(rhot);
				}else{
					std::vector<arma::cx_mat> rhot;
					rhot.push_back(inp.rho0);
					inp.Nbeads = inp.Nbeadsweep;
					double sweep_dt = inp.sweep_tf/inp.L;
					for(int k=0;k<inp.L;k++)
					{
						inp.dt = sweep_dt*(k+1)/(inp.Nbeadsweep-1);
						std::pair<arma::cx_mat,arma::cx_mat> cetas = 
							sambarta_area::eta_coeff_wapper(inp.omegas, inp.jdw, inp.Nbeads, inp.beta, inp.hbar, inp.dt);

						std::vector<arma::cx_mat> crhot = lite_quapi(inp, cetas.first, cetas.second);

						rhot.push_back(crhot.back());
						
						if(mpirank==0)
						{
							std::cout<<"#";
							std::cout.flush();
						}
					}
					sweep_data.push_back(rhot); 

				}

				if(mpirank==0)
					std::cout<<" ...done."<<std::endl;
			}

		if(mpirank==0){
			outsweep("sweep.xml",sweep_data,inp);
			outsweep_human("RDM.dat",sweep_data,inp);
		}
	}
	MPI_Finalize();
}