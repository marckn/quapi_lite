#ifndef __INPUT__
#define __INPUT__
#include<iostream>
#include<armadillo>

class Input
{
public:
	Input(std::string filename);

	double beta, dt, hbar;
	int Nbeads;
	arma::vec omegas, jdw;
	arma::mat H0;
	double H0diagpart;

	bool sweep;  // do all "rho0 basis" for DQME
	arma::cx_mat rho0;
	arma::mat dvr_values;

	int Nstates,dvr_dim;
	int L, Nbeadsweep;
	double sweep_tf;
};

#endif