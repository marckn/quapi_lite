#include "path_counter.h"

// warning: i didn't bother to put any boundary check. 
// Just do your index math and be a nice guy.


PathCounter::PathCounter(const std::string& src, int _nlevels)
{
	
	memlevel = src.size();
	
	int intarr[memlevel];
	
	
	
	for(int i=0;i<memlevel;i++)
	{
		std::stringstream conv;
		conv<<src[i];
		conv>>intarr[i];
		status.push_back(intarr[i]);
	}
	
	int nnlevels=0;
	for(int i=0;i<memlevel;i++)
		nnlevels = std::max(nnlevels,intarr[i]);
		
	
	nlevels=std::max(_nlevels,nnlevels);
	deltapos=0;
	
	
	
	
}

void PathCounter::jump(unsigned long int ivl)
{
	unsigned long int itmp=ivl;
	for(int j=0;j<memlevel;j++)
	{
		unsigned long int ipo =std::pow((long double)nlevels,(long double)(memlevel-j-1));
		status[j] = itmp / ipo;
		itmp = itmp - status[j]*std::pow((long double)nlevels,(long double)(memlevel-j-1));
	}
}

PathCounter& PathCounter::operator++()
{
	for(int i=memlevel-1;i>=0;i--)
	{
		deltapos=i;
		status[i]++;
		if(status[i] < nlevels)
			break;
		
		status[i]=0;
	}
	
	return *this;
}

PathCounter& PathCounter::operator--()
{
	for(int i=memlevel-1;i>=0;i--)
	{
		deltapos=i;
		status[i]--;
		if(status[i] >= 0)
			break;
		
		status[i]=nlevels-1;
	}
	
	return *this;
}

PathCounter& PathCounter::operator=(const PathCounter& src)
{
	if(this != &src)
	{
		nlevels=src.nlevels;
		memlevel=src.memlevel;
		status=src.status;
		deltapos=0;
	}
	return *this;
}

PathCounter& PathCounter::operator=(const std::string& src)
{
	
	PathCounter np(src);
	nlevels=np.nlevels;
	memlevel=np.memlevel;
	status=np.status;
	deltapos=0;
	
	return *this;
	
}


bool PathCounter::operator==(const PathCounter& src)
{
	if(src.nlevels != nlevels || src.memlevel != memlevel)
		return false;
		
	for(int i=0;i<memlevel;i++)
		if(src.status[i] != status[i])
			return false;
	
	return true;
}

unsigned long int PathCounter::getIdx() const
{
	unsigned long int idx=0;
	for (int i=0;i<memlevel;i++)
		idx += status[i]*std::pow((long double)nlevels,(long double)(memlevel-1-i));
	
	return idx;
	
}


std::ostream& operator<<(std::ostream& out, const PathCounter& src)
{
	out<<"[  ";
	for(int i=0;i<src.memlevel;i++)
		out<<src.status[i]<<" ";
	
	out<<" ] -- idx "<< src.getIdx()<<" -- ";
	
	return out;
}


