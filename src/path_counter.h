#ifndef __PCOUNTER__
#define __PCOUNTER__

#include<iostream>
#include<sstream>
#include<cmath>
#include<vector>

class PathCounter
{
	public:
		PathCounter():
			nlevels(0),
			memlevel(0),
			deltapos(0){};
		
		PathCounter(int _nlevels, int _memlevel)
		{
			nlevels=_nlevels;
			memlevel=_memlevel;
			deltapos=0;
			status.resize(memlevel,0);
		}
		
		PathCounter(const std::string& src, int _nlevels=0);
		
		PathCounter(const std::vector<int>& arr, int _nlevels=0)
		{
			nlevels=_nlevels;
			memlevel = arr.size();
			status=arr;
			deltapos=0;
		}
			
		
		void initialize(int _nlevels, int _memlevel){
			PathCounter(_nlevels,_memlevel);
		}
		
		PathCounter& operator++();
		PathCounter& operator++(int){return operator++();}
		
		PathCounter& operator--();
		PathCounter& operator--(int){return operator--();}
		
		bool operator==(const PathCounter& src);
		PathCounter& operator=(const PathCounter& src);
		PathCounter& operator=(const std::string& src);
		friend std::ostream& operator<<(std::ostream& out, const PathCounter& src);
		int operator[](int i) {return status[i];}  
		
		void clear()
		{
			status.clear();
			status.resize(memlevel,0);
			deltapos=0;
		}
		
		void resetMaxLevel(int _nlevels)
		{	
			nlevels=_nlevels;
			clear();
		}
		
		
		unsigned long int getIdx() const;
		
		int getDeltaFrom() const{
			return deltapos;
		}
		
		void jump(unsigned long int ivl);
		
		std::vector<int> cloneStatus(){return status;}
		
	private:
		int nlevels, memlevel;
		std::vector<int> status;
		int deltapos;
};

#endif
