#include "quapi.h"
#include "path_counter.h"
#include<cmath>
#include<mpi.h>

std::vector<arma::cx_mat> lite_quapi(Input& inp, arma::cx_mat& eta_inner, arma::cx_mat& eta_outer)
{
	int nstates = inp.Nstates;
	int nlevels = nstates*nstates;
	int nbeads = inp.Nbeads;
	
	int mpirank,nranks;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);
	
	arma::cx_mat prop = arma::expmat(std::complex<double>(0,-inp.dt/inp.hbar)*inp.H0);
	arma::cx_mat propc = prop.t();
	
	// init forward-backward levels
	std::vector<std::pair<int,int>> FBlevels;
	for(int i=0;i<nstates;i++)
		for(int j=0;j<nstates;j++)
		{
			std::pair<int,int> clv = std::make_pair(i,j);
			FBlevels.push_back(clv);
		}

	arma::cx_mat rht(inp.Nstates,inp.Nstates,arma::fill::zeros);
	std::vector<arma::cx_mat> retval(inp.Nbeads,rht);
	retval[0] = inp.rho0;


	PathCounter paths(nlevels,nbeads);
	if(std::pow((long double)nlevels,nbeads) > std::numeric_limits<unsigned long int>::max())
	{
		std::cout<<"Number of paths exceeds numeric limit"<<std::endl;
		std::exit(1);
	}
	unsigned long int npaths = std::pow((long double)nlevels,nbeads);
	for(unsigned long int i=0;i<npaths;i++)
	{
		std::complex<double> freep_weight=inp.rho0(FBlevels[paths[0]].first,FBlevels[paths[0]].second);
		
		// later add this for minimal path filtering. With sweeping and parallelization we need a smarter sheme
		if(std::abs(freep_weight)<1E-14){
			paths++;
			continue;
		}
		//std::cout<<paths<<std::endl;
		double s0f = inp.dvr_values(FBlevels[paths[0]].first);
		double s0b = inp.dvr_values(FBlevels[paths[0]].second);
		double deltas0 = s0f - s0b;

		std::complex<double> eta_acc=deltas0*(s0f*eta_inner(0,0) - s0b*std::conj(eta_inner(0,0)));
		int startfrom = paths.getDeltaFrom();
		for(int j=1;j<nbeads;j++)
		{
			int jfow = FBlevels[paths[j]].first;
			int jmfow = FBlevels[paths[j-1]].first;
			int jback = FBlevels[paths[j]].second;
			int jmback = FBlevels[paths[j-1]].second;

			std::complex<double> cw = prop(jfow,jmfow)*propc(jmback,jback);
			freep_weight *= cw;

			double sjf = inp.dvr_values(FBlevels[paths[j]].first);
			double sjb = inp.dvr_values(FBlevels[paths[j]].second);
			double deltasj = sjf - sjb;
			
			if(j>=startfrom)
			{
				std::complex<double> ceta = eta_acc;
				for(int kp=0;kp<=j;kp++)
				{
					double skpf = inp.dvr_values(FBlevels[paths[kp]].first);
					double skpb = inp.dvr_values(FBlevels[paths[kp]].second);
					ceta += deltasj*(skpf*eta_outer(j-1,kp) - skpb*std::conj(eta_outer(j-1,kp))); 
				}
				retval[j](jfow,jback) += freep_weight*std::exp(-ceta/inp.hbar);
			}

			for(int kp=0;kp<=j;kp++)
			{
				double skpf = inp.dvr_values(FBlevels[paths[kp]].first);
				double skpb = inp.dvr_values(FBlevels[paths[kp]].second);
				eta_acc += deltasj*(skpf*eta_inner(j,kp) - skpb*std::conj(eta_inner(j,kp))); 
			}
		}
		paths++;
	}


	return retval;
}