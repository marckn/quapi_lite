#include "eta_coeffs.h"

namespace sambarta_area
{
std::complex<double> I = std::complex<double>(0,1);
double pi = acos(-1.0);

std::complex<double> integrate(arma::cx_vec& integrand, arma::vec& freq)
{
	double dw = freq(1)-freq(0);
	std::complex<double> integ= 0.5*(integrand(0)+integrand(freq.n_elem-1));
	for(int i=1;i<freq.n_elem-1;i++)
		integ += integrand(i);
	
	return integ*dw;
}

//THESE FUNCS TO BE EXECUTED JUST ONCE	
void eta_mids (arma::cx_vec &eta_mm, size_t N, arma::vec &freq, arma::vec &J, double beta, double hbar, double dt) {
	eta_mm = arma::zeros<arma::cx_vec>(N);
	arma::vec coef_kkp = (J /(freq % freq)) % (arma::exp(hbar*freq*beta/2.) / arma::sinh(hbar*freq*beta/2.)) % arma::sin(freq*dt/2.)%arma::sin(freq*dt/2.);
	arma::vec coef_kk = (J /(freq%freq)) % (arma::exp(hbar*freq*beta/2.) / arma::sinh(hbar*freq*beta/2.));
	arma::cx_vec kk_to_integ = coef_kk % (1.0-arma::exp(-I*freq*dt));
	eta_mm(0) = (1.0/(2.*pi)) * integrate(kk_to_integ,freq);//eta_kk
	for (unsigned i=1;i<eta_mm.n_rows;i++) {
		arma::cx_vec kkp_to_integ = coef_kkp % arma::exp(-I*freq*dt*i);
		eta_mm(i) = (2./pi) * integrate(kkp_to_integ,freq);//eta_kk'
	}
}

void eta_mid_ends (arma::cx_vec &eta_me, size_t N, arma::vec &freq, arma::vec &J, double beta, double hbar, double dt) {
	eta_me = arma::zeros<arma::cx_vec>(N);
	arma::vec coef = (J /(freq%freq)) % (arma::exp(hbar*freq*beta/2.) / arma::sinh(hbar*freq*beta/2.)) % arma::sin(freq*dt/2.) % arma::sin(freq*dt/4.);
	for (unsigned i=0;i<eta_me.n_rows;i++) {
		arma::cx_vec to_integ = coef % arma::exp(-I*freq*(i+1-0.25)*dt);
		eta_me(i) = (2./pi) * integrate(to_integ,freq);//eta_Nk'
	}
}

void eta_ends (arma::cx_vec &eta_ee, size_t N, arma::vec &freq, arma::vec &J, double beta, double hbar, double dt) {
	eta_ee = arma::zeros<arma::cx_vec>(N);
	arma::vec coef = (J /(freq%freq)) % (arma::exp(hbar*freq*beta/2.) / arma::sinh(hbar*freq*beta/2.)) % arma::sin(freq*dt/4.) % arma::sin(freq*dt/4.);
	for (unsigned i=0;i<eta_ee.n_rows;i++) {
		arma::cx_vec to_integ = coef % arma::exp(-I*freq*(i+1-0.5)*dt);
		eta_ee(i) = (2./pi) * integrate(to_integ,freq);//eta_N0
	}
}

void eta_0 (std::complex<double> &eta_00, arma::vec &freq, arma::vec &J, double beta, double hbar, double dt) {
	arma::cx_vec to_integ = (J/(freq%freq)) % (arma::exp(hbar*freq*beta/2.) / arma::sinh(hbar*freq*beta/2.)) % (1 - arma::exp(-I*freq*dt/2.));
	eta_00 = (1./(2.*pi)) * integrate(to_integ,freq);
}

void calc_eta_coeffs ( arma::cx_vec &eta_mm, arma::cx_vec &eta_me, arma::cx_vec &eta_ee, std::complex<double> &eta_00, size_t N, arma::vec &freq, arma::vec &J, double beta, double hbar, double dt) {
	eta_mids(eta_mm,N,freq,J,beta,hbar,dt);
	eta_mid_ends(eta_me,N,freq,J,beta,hbar,dt);
	eta_ends(eta_ee,N,freq,J,beta,hbar,dt);
	eta_0(eta_00,freq,J,beta,hbar,dt);
}


std::pair<arma::cx_mat,arma::cx_mat> eta_coeff_wapper(arma::vec& freq, arma::vec& jdw, int Nmax, double beta, double hbar, double dt)
{
	arma::cx_vec eta_mm,eta_me,eta_ee;
	std::complex<double> eta_00;

	eta_mids(eta_mm,Nmax,freq,jdw,beta,hbar,dt);
	eta_mid_ends(eta_me,Nmax,freq,jdw,beta,hbar,dt);
	eta_ends(eta_ee,Nmax,freq,jdw,beta,hbar,dt);
	eta_0(eta_00,freq,jdw,beta,hbar,dt);

	arma::cx_mat eta_inner(Nmax,Nmax,arma::fill::zeros);
	arma::cx_mat eta_outer(Nmax,Nmax+1,arma::fill::zeros);

	eta_inner(0,0) = eta_00;
	for(int i=1;i<Nmax;i++)
	{
		for(int j=1;j<Nmax;j++)
		{
			int idst = std::abs(i-j);
			eta_inner(i,j) = eta_mm(idst);
		}
		eta_inner(0,i) = eta_inner(i,0) = eta_me(i-1);   // check here
	}

	// outer part, for all N up to Nmax. First index in outer means +1, like: eta_outer(0,0) is relative to N=1 and 0
	for(int i=0;i<Nmax;i++)
	{
		eta_outer(i,0) = eta_ee(i);
		eta_outer(i,i+1) = eta_00;
		for(int j=1;j<=i;j++)
			eta_outer(i,j) = eta_me(i-j);   // check here
	}

	return std::make_pair(eta_inner,eta_outer);

}


}