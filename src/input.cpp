#include "input.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace pt = boost::property_tree;
Input::Input(std::string fname)
{
	pt::ptree tree;
    pt::read_xml(fname,tree);

    beta = tree.get<double>("beta");
 	dt = tree.get<double>("dt");
    hbar = tree.get<double>("hbar",1);
    Nbeads = tree.get<int>("Nbeads");
    H0diagpart=0;

    std::string ans = tree.get<std::string>("sweep.<xmlattr>.status","NO");
    sweep=false;
    if(ans=="YES" || ans=="yes" || ans=="1" || ans=="ON" || ans=="on")
    {
    	sweep=true;
        L = tree.get<int>("sweep.L",-1);
        Nbeadsweep=tree.get<int>("sweep.N",Nbeads);
        sweep_tf = tree.get<double>("sweep.memory_time",dt*Nbeads);
    }

    Nstates = tree.get<int>("dvrs.<xmlattr>.nstates");
    dvr_dim = tree.get<int>("dvrs.<xmlattr>.dim",1);
    dvr_values.zeros(Nstates,dvr_dim);
    H0.zeros(Nstates,Nstates);
    rho0.zeros(Nstates,Nstates);

    for(int i=0;i<Nstates;i++)
    {
    	std::string xtag = "dvrs.level_"+std::to_string(i);
    	std::string tmpstr = tree.get<std::string>(xtag);
    	std::stringstream ggt(tmpstr);
    	for(int d=0;d<dvr_dim;d++)
    		ggt>>dvr_values(i,d);

    	//dvr_values(i) = tree.get<double>(xtag);
    }

    for(int i=0;i<Nstates;i++)
    	for(int j=0;j<Nstates;j++)
    	{
    		std::string xtag = "H0.elem_"+std::to_string(i)+"_"+std::to_string(j);
    		if(boost::optional<double> v = tree.get_optional<double>(xtag))
    			H0(i,j)= H0(j,i)= *v;

    	}

    if(!sweep)
    	for(int i=0;i<Nstates;i++)
    		for(int j=0;j<Nstates;j++)
    		{
    			std::string xtag = "rho0.elem_"+std::to_string(i)+"_"+std::to_string(j);
    			if(boost::optional<std::complex<double>> v = tree.get_optional<std::complex<double>>(xtag))
    			{
    				rho0(i,j)= *v;
    				rho0(j,i)= std::conj(*v);
    			}
    		}
    
    int nws = tree.get<int>("JdW.<xmlattr>.nw");
    omegas.zeros(nws);
    jdw.zeros(nws);
    for(int i=0;i<nws;i++)
    {
    	omegas(i)=tree.get<double>("JdW.elem_"+std::to_string(i)+".<xmlattr>.w");
    	jdw(i) = tree.get<double>("JdW.elem_"+std::to_string(i));
        double coupquad = 2*jdw(i)*omegas(i)/acos(-1);
        H0diagpart += 0.5*coupquad/(omegas(i)*omegas(i));

    }
       
}

