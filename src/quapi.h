#ifndef __QUAPI__
#define __QUAPI__
#include<armadillo>
#include "input.h"

std::vector<arma::cx_mat> lite_quapi(Input& inp, arma::cx_mat& eta_inner, arma::cx_mat& eta_outer);

double dvr_prod(const arma::rowvec& p1, const arma::rowvec& p2);

#endif