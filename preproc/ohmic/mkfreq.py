#!/usr/bin/env python2
import math

fname="freqs_nancy.xml"

wc = 5.0
xi= 0.3
beta=5
hbar=1.0
dt=0.125


nws = 10000
nrange=20

w0 = -nrange*wc
wf = nrange*wc

dw = (wf-w0)/(nws-1)

standard_header="""
<Nbeads>5</Nbeads>
<dvrs nstates="2">
	<level_0>1</level_0>
	<level_1>-1</level_1>
</dvrs>

<H0>
	<elem_0_1>-1</elem_0_1>
</H0>

<rho0>
	<elem_0_0>1</elem_0_0>
</rho0>
<sweep status="YES">
<!--        <L>8</L>
        <N>9</N>
        <memory_time>2</memory_time>-->
</sweep>
"""

with open(fname,"w+") as f:
	f.write("<?xml version=\"1.0\"?>\n")
	f.write(standard_header)
	f.write("<dt>{0}</dt>\n<hbar>{1}</hbar>\n<beta>{2}</beta>\n<wc>{3}</wc>\n<xi>{4}</xi>\n".format(dt,hbar,beta,wc,xi))
	f.write("<JdW nw=\"{0}\">\n".format(nws))
	for i in xrange(nws):
		cw = w0 + i*dw
		if(abs(cw)<1E-6):
			print "Warning: using a w that is dangerously close to 0"
		Jw = 0.5*math.acos(-1)*hbar*xi*cw*math.exp(-abs(cw)/wc)

		f.write("\t<elem_{0} w=\"{1}\">{2}</elem_{0}>\n".format(i,cw,Jw))

	f.write("</JdW>\n")



