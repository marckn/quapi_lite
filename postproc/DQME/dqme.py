#!/usr/bin/env python2
import numpy as np
from scipy.linalg import expm
import xml.etree.ElementTree as etree

tf = 500.0
DTDQME = 1   # dt_dqme/dt_quapi

tree = etree.parse('sweep.xml')
root = tree.getroot()

general_properties = root.find('general_properties')

nstates = int(general_properties.find('nstates').text)
nbeads = int(general_properties.find('nbeads').text)
dt = float(general_properties.find('dt').text)
hbar = float(general_properties.find('hbar').text)
nlevels = nstates*nstates

ham = np.ndarray((nstates,nstates),dtype=float)

hamiltonian = root.find('hamiltonian')
for i in xrange(nstates):
	for j in xrange(nstates):
		ctag = "elem_"+str(i)+"_"+str(j)
		ham[(i,j)] = float(hamiltonian.find(ctag).text)


print "Found {0} states, {1} time-points. dt is {2}".format(nstates,nbeads,dt)

print "System Hamiltonian:"
print ham


rho0_node = root.find('rho_projection')
proj = np.zeros((nlevels,),dtype=complex)

if rho0_node is None:
	proj[(0,)]=1.0
	print "No projection rho given: using default with 1 on (0,0) and 0 elsewhere"
else:
	rho0p = np.zeros((nstates,nstates),dtype=complex)
	for i in xrange(nstates):
		for j in xrange(nstates):
			ctag = "elem_"+str(i)+"_"+str(j)
			rho0p[(i,j)]=complex(rho0_node.find(ctag).text)


	proj = rho0p.flatten()
	print "Projecting to rho0:",rho0p






dvr_values = []
dvr_node = root.find('dvr_defs')
hamdiag = float(dvr_node.attrib['diagterm'])
dvr_dim = int(dvr_node.attrib['dvrdim'])

for i in xrange(nstates):
	cval=dvr_node.find('state_'+str(i)).text
	vals = cval.split(" ")
	vals = [x for x in vals if x !='']
	vals = [float(x) for x in vals if x !='\t']


	dvr_values.append(vals)
	
	dprod=0.0
	for v in vals:
		dprod += float(v)*float(v)

	ham[(i,i)] -= hamdiag*dprod

print "Added diagonal balancing on H0 (important if DVRs >2 or not symmetric around 0)"


print "DVR states: ",dvr_values


def printout(M,title):
	print title
	nrows = M.shape[0]
	ncols = M.shape[1]
	for i in xrange(nrows):
		ln=""
		for j in xrange(ncols):
			ln = ln +"{0} \t ".format(M[(i,j)])

		print ln

	print "\n"




print "Now reading sweep... ",
sweep = root.find('sweep')
rhos=[]
for i in xrange(nbeads):
	ctag = "time_index_"+str(i)
	time_node = sweep.find(ctag)
	crho = np.ndarray((nlevels,nlevels),dtype=complex)
	for j in xrange(nlevels):
		ctag = "sweep_sector_"+str(j)
		sector_node = time_node.find(ctag)
		knt=0
		for l in xrange(nstates):
			for k in xrange(nstates):
				ctag = 'rhoelem_'+str(l)+"_"+str(k)
				rhoelem_node = sector_node.find(ctag)
				repart = float(rhoelem_node.find('real').text)
				imgpart = float(rhoelem_node.find('imag').text)
				crho[(knt,j)] = complex(repart,imgpart)
				knt=knt+1


	rhos.append(crho)


print "...done."

if DTDQME != 1:
	print "Selecting a subset of the rdm sweep"
	rhos_sparse=[]
	for i in xrange(0,len(rhos),DTDQME):
		print "keeping bead ",i
		rhos_sparse.append(rhos[i])

	dt = dt*DTDQME
	nbeads = len(rhos_sparse)
	rhos = rhos_sparse
	print "New dt = ",dt
	print "Beads retained: ",nbeads



Hexp = expm(complex(0,dt/hbar)*ham)
Hexpt = Hexp.conj().T

Gmatdag = np.zeros((nstates**2,nstates**2),dtype=complex)

for i1 in xrange(nstates):
	for i2 in xrange(nstates):
		for j1 in xrange(nstates):
			for j2 in xrange(nstates):
				i = i1*nstates + i2
				j = j1*nstates + j2

				Gmatdag[(i,j)] = Hexp[(i2,j2)]*Hexpt[(j1,i1)]
				

Gmat = Gmatdag.conj().T


def Gaction(rho):
	Tt = np.matmul(Gmat,rho)
	return Tt


#printout(Gmat,"G")

#printout(rhos[1],"rho_1")

Tele=[]
T1 = Gaction(rhos[1])
Tele.append(T1)


#printout(T1,"T1, t={0}".format(dt))

#printout(rhos[1], "rho_{0}".format(1))



for i in xrange(2,nbeads):
	Tt = Gaction(rhos[i])
	for j in xrange(1,i):
		Tt = Tt - np.matmul(Tele[j-1],rhos[i-j])

	#printout(Tt,"T{0}, t={1}".format(i,i*dt))
	#printout(rhos[i], "rho_{0}".format(i))
	Tele.append(Tt)

ct = dt*nbeads
Niter = int((tf-ct)/dt)


for i in xrange(Niter):
	Tsum = np.matmul(Tele[0],rhos[nbeads+i-1])
	for k in xrange(2,nbeads):
		Tsum = Tsum + np.matmul(Tele[k-1],rhos[nbeads+i-k])
		


	Tsum = np.matmul(Gmatdag,Tsum)	
	rhos.append(Tsum)
	#printout(Tsum,"rho_L+{0}".format(i))


with open("densm.dat","w+") as f:
	header="#1)time \t "
	knt=2
	for i in xrange(nstates):
		for j in xrange(nstates):
			header += "{0})Re[{1}{2}] {3})Img[{1}{2}] \t ".format(knt,i,j,knt+1)
			knt += 2


	header += "\n"
	f.write(header)


	for i,rho in enumerate(rhos):
		ct = i*dt
		crho = np.matmul(rho,proj)
		lout = "{0} \t ".format(ct)
		for j in xrange(nlevels):
			lout += "{0} {1} \t".format(crho.real[(j,)],crho.imag[(j,)])

		f.write(lout+"\n")
