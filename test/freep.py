#!/bin/env python2 

import numpy as np
from scipy.linalg import expm as expm
import scipy.constants as c

hbar = c.hbar/(c.e*1E-12)

H = np.ndarray((3,3),dtype=float)

H[(0,0)] = 0.0310335380817
H[(1,1)] = 0.0
H[(2,2)] = 0.0310335380815
H[(0,1)] = -0.000566428350735
H[(0,2)] = 0.000242395561884
H[(1,2)] = -0.000566428350734
H[(1,0)] = H[(0,1)]
H[(2,0)] = H[(0,2)]
H[(2,1)] = H[(1,2)]




dt = 0.3
arg = complex(0,1.0)*dt*H/hbar

Pf = expm(-arg)
Pb = expm(arg)

print Pf
print Pb

rho0 = np.zeros((3,3),dtype=float)

rho0[(0,0)] = 1.0



f = open("pops.dat","w+")
for i in xrange(50):
   ct = dt*i
   arg = complex(0,1.0)*ct*H/hbar
   
   Pf = expm(-arg)
   Pb = expm(arg)
 
   tm1= np.matmul(Pf,rho0)
   
   rhot = np.matmul(tm1,Pb)
   f.write("{0}  {1} {2} {3}\n".format(
                  ct,rhot[(0,0)].real,
                  rhot[(1,1)].real,
                  rhot[(2,2)].real))
   #print rhot
